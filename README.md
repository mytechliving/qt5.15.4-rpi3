# QT5.15.4 -Raspbian-bullseye

Automated installation scripts to compile QT5 on Raspbian Bullseye for Raspberry PI with EGLFS support
This whole process is expected to take a long time. I would not recommend watching the install, rather check back every few hours to verify nothing has gone wrong.
Compile scripts for the Raspberry Pi3 (armV8)
The script is based on this tutorial http://www.tal.org/tutorials/building-qt-58-raspberry-pi-debian-stretch and was cloned from https://github.com/MarkusIppy/QT5.x-raspbian-stretch and further modified by me.

Usage :
Download raspbian Bullseye from:
https://www.raspberrypi.com/software/operating-systems/
https://downloads.raspberrypi.org/raspios_lite_armhf/images/
https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2022-04-07/2022-04-04-raspios-bullseye-armhf-lite.img.xz

Install the downloaded image onto your SD card (the lite version will be enough) 

After booting up your PI, follow the steps below :

Increase Rpi's swap size. As root, edit the file /etc/dphys-swapfile and modify the variable CONF_SWAPSIZE
```
$sudo apt-get install vim
$sudo vim /etc/dphys-swapfile
```
To enter insert mode, click "i" on the keyboard and then use the arrow keys to navigate to conf_swapsize
change this to "CONF_SWAPSIZE=2048"
Once the change has been made, hit the escape key on the keyboard which will bring you back into the command mode. 
In command mode, type the following     :wq!    This will write the file and quit the VIM editor without prompting for anything more. This command is powerful and any changes that were made will be saved, so be careful not to mess anything up.


Run dphys-swapfile setup which will create and initialize the file.
```
$ sudo dphys-swapfile swapon
```
Source : https://wpitchoune.net/tricks/raspberry_pi3_increase_swap_size.html

After increasing the swap size, you'll need to update apt-get and install Git.

Update apt
```
$ sudo apt-get update
$ sudo apt-get upgrade
```

Now you need to Install GIT
```
$ sudo apt-get install git
```

Clone this repo with the following
```
$ git clone https://gitlab.com/mytechliving/qt5.15.4-rpi3.git
```

Navigate to the appropriate directory
```
$ cd QT5.15.4 - RPi3 
```

now, in order to compile and install QT5.15.4 on the RPi3, run the following command which is found within the QT5 direcroty.

```
$sudo chmod +x Pi3buildQT5.15.4.sh
$./Pi3buildQT5.15.4.sh
```

```
Configure summary:

Building on: linux-g++ (arm, CPU features: <none>)
Building for: devices/linux-rasp-pi2-g++ (arm, CPU features: neon)
Configuration: cross_compile use_gold_linker enable_new_dtags largefile neon precompile_header shared rpath release c++11 c++14 c++1z concurrent dbus reduce_exports stl
Build options:
  Mode ................................... release
  Optimize release build for size ........ no
  Building shared libraries .............. yes
  Using C++ standard ..................... C++1z
  Using ccache ........................... no
  Using gold linker ...................... yes
  Using new DTAGS ........................ yes
  Using precompiled headers .............. yes
  Using LTCG ............................. no
  Target compiler supports:
    NEON ................................. yes
  Build parts ............................ libs
Qt modules and options:
  Qt Concurrent .......................... yes
  Qt D-Bus ............................... yes
  Qt D-Bus directly linked to libdbus .... yes
  Qt Gui ................................. yes
  Qt Network ............................. yes
  Qt Sql ................................. yes
  Qt Testlib ............................. yes
  Qt Widgets ............................. yes
  Qt Xml ................................. yes
Support enabled for:
  Using pkg-config ....................... yes
  QML debugging .......................... yes
  udev ................................... yes
  Using system zlib ...................... yes
Qt Core:
  DoubleConversion ....................... yes
    Using system DoubleConversion ........ no
  GLib ................................... yes
  iconv .................................. no
  ICU .................................... yes
  Logging backends:
    journald ............................. no
    syslog ............................... no
    slog2 ................................ no
  Using system PCRE2 ..................... no
Qt Network:
  getaddrinfo() .......................... yes
  getifaddrs() ........................... yes
  IPv6 ifname ............................ yes
  libproxy ............................... no
  OpenSSL ................................ yes
    Qt directly linked to OpenSSL ........ no
  SCTP ................................... no
  Use system proxies ..................... yes
Qt Gui:
  Accessibility .......................... yes
  FreeType ............................... yes
    Using system FreeType ................ yes
  HarfBuzz ............................... yes
    Using system HarfBuzz ................ no
  Fontconfig ............................. yes
  Image formats:
    GIF .................................. yes
    ICO .................................. yes
    JPEG ................................. yes
      Using system libjpeg ............... yes
    PNG .................................. yes
      Using system libpng ................ yes
  EGL .................................... yes
  OpenVG ................................. no
  OpenGL:
    Desktop OpenGL ....................... no
    OpenGL ES 2.0 ........................ yes
    OpenGL ES 3.0 ........................ no
    OpenGL ES 3.1 ........................ no
  Session Management ..................... yes
Features used by QPA backends:
  evdev .................................. yes
  libinput ............................... yes
  INTEGRITY HID .......................... no
  mtdev .................................. no
  tslib .................................. no
  xkbcommon-evdev ........................ yes
QPA backends:
  DirectFB ............................... no
  EGLFS .................................. yes
  EGLFS details:
    EGLFS i.Mx6 .......................... no
    EGLFS i.Mx6 Wayland .................. no
    EGLFS EGLDevice ...................... no
    EGLFS GBM ............................ no
    EGLFS Mali ........................... no
    EGLFS Raspberry Pi ................... yes
    EGL on X11 ........................... no
  LinuxFB ................................ yes
  VNC .................................... yes
  Mir client ............................. no
  X11:
    Using system-provided XCB libraries .. no
    EGL on X11 ........................... no
    Xinput2 .............................. no
    XCB XKB .............................. yes
    XLib ................................. no
    XCB render ........................... yes
    XCB GLX .............................. no
    XCB Xlib ............................. no
    Using system-provided xkbcommon ...... no
Qt Widgets:
  GTK+ ................................... no
  Styles ................................. Fusion Windows
Qt PrintSupport:
  CUPS ................................... yes
Qt Sql:
  DB2 (IBM) .............................. no
  InterBase .............................. no
  MySql .................................. yes
  OCI (Oracle) ........................... no
  ODBC ................................... no
  PostgreSQL ............................. yes
  SQLite2 ................................ no
  SQLite ................................. yes
    Using system provided SQLite ......... no
  TDS (Sybase) ........................... no
Qt SerialBus:
  Socket CAN ............................. yes
  Socket CAN FD .......................... yes
QtXmlPatterns:
  XML schema support ..................... yes
Qt QML:
  QML interpreter ........................ yes
  QML network support .................... yes
Qt Quick:
  Direct3D 12 ............................ no
  AnimatedImage item ..................... yes
  Canvas item ............................ yes
  Support for Qt Quick Designer .......... yes
  Flipable item .......................... yes
  GridView item .......................... yes
  ListView item .......................... yes
  Path support ........................... yes
  PathView item .......................... yes
  Positioner items ....................... yes
  ShaderEffect item ...................... yes
  Sprite item ............................ yes
Qt Gamepad:
  SDL2 ................................... no
Qt 3D:
  Assimp ................................. yes
  System Assimp .......................... no
  Output Qt3D Job traces ................. no
  Output Qt3D GL traces .................. no
Qt 3D GeometryLoaders:
  Autodesk FBX ........................... no
Qt Bluetooth:
  BlueZ .................................. yes
  BlueZ Low Energy ....................... yes
  Linux Crypto API ....................... yes
Qt Sensors:
  sensorfw ............................... no
Qt Quick Controls 2:
  Styles ................................. Default Material Universal
Qt Quick Templates 2:
  Hover support .......................... yes
  Multi-touch support .................... yes
Qt Positioning:
  Gypsy GPS Daemon ....................... no
  WinRT Geolocation API .................. no
Qt Location:
  Geoservice plugins:
    OpenStreetMap ........................ yes
    HERE ................................. yes
    Esri ................................. yes
    Mapbox ............................... yes
    MapboxGL ............................. yes
    Itemsoverlay ......................... yes
Qt Multimedia:
  ALSA ................................... yes
  GStreamer 1.0 .......................... yes
  GStreamer 0.10 ......................... no
  Video for Linux ........................ yes
  OpenAL ................................. no
  PulseAudio ............................. yes
  Resource Policy (libresourceqt5) ....... no
  Windows Audio Services ................. no
  DirectShow ............................. no
  Windows Media Foundation ............... no

Qt is now configured for building. Just run 'make'.
Once everything is built, you must run 'make install'.
Qt will be installed into '/opt/Qt5.9'.

Prior to reconfiguration, make sure you remove any leftovers from
the previous build.
```

Potential issues:
After a successful Make, during the install, if it fails during "sudo make install" with an error stating "/usr/bin/ld: cannot find -lqt_clip2tri", you will need to do the following: 
```
// cd to each directory and do 'sudo make' to each of them:
~/build/qtlocation/src/3rdparty/clip2tri
~/build/qtlocation/src/3rdparty/clipper
~/build/qtlocation/src/3rdparty/poly2tri
```

Javascript Core Compatibility issues
Navigate to the  QT5 Source directory and drill down to the following file, opening it in VIM.
```
QT5/src/3rdparty/javascriptcore/JavaScriptCore/wtf/Platform.h
```
Scroll down to line 306,6 +306,9, looking for the following:
```
|| defined(ARM_ARCH_7R)
#define WTF_ARM_ARCH_VERSION 7
```
You'll now need to add in the following by entering Insert mode by hitting the letter "i"
```
#elif defined(__ARM_ARCH_8__) \
    || defined(__ARM_ARCH_8A__)
#define WTF_ARM_ARCH_VERSION 8
```
Once added, hit escape and save using :wq!


If everything is working you can delete the build folder as well as the source code folder 

Happy coding :-)
